// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.normandy.api_url", "");
user_pref("app.normandy.first_run", false);
user_pref("app.normandy.migrationsApplied", 10);
user_pref("app.update.disabledForTesting", true);
user_pref("app.update.lastUpdateTime.addon-background-update-timer", 0);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1615903267);
user_pref("app.update.lastUpdateTime.recipe-client-addon-run", 1615903239);
user_pref("app.update.lastUpdateTime.region-update-timer", 0);
user_pref("app.update.lastUpdateTime.rs-experiment-loader-timer", 1615903239);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 0);
user_pref("app.update.lastUpdateTime.services-settings-poll-changes", 0);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 0);
user_pref("apz.content_response_timeout", 60000);
user_pref("browser.bookmarks.addedImportButton", true);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.contentblocking.category", "standard");
user_pref("browser.contentblocking.introCount", 99);
user_pref("browser.download.panel.shown", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.svg", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.webp", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.xml", true);
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1615903237);
user_pref("browser.laterrun.bookkeeping.sessionCount", 1);
user_pref("browser.laterrun.enabled", true);
user_pref("browser.migration.version", 104);
user_pref("browser.newtabpage.activity-stream.impressionId", "{e4edd927-27bf-45c3-a144-aa67be3811ea}");
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pageActions.persistedActions", "{\"version\":1,\"ids\":[\"bookmark\",\"pinTab\",\"bookmarkSeparator\",\"copyURL\",\"emailLink\",\"addSearchEngine\",\"sendToDevice\",\"pocket\",\"screenshots_mozilla_org\"],\"idsInUrlbar\":[\"pocket\",\"bookmark\"]}");
user_pref("browser.pagethumbnails.capturing_disabled", true);
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.safebrowsing.blockedURIs.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.search.update", false);
user_pref("browser.sessionstore.resume_from_crash", false);
user_pref("browser.shell.mostRecentDateSetAsDefault", "1615903239");
user_pref("browser.slowStartup.averageTime", 1404);
user_pref("browser.slowStartup.samples", 1);
user_pref("browser.startup.homepage_override.buildID", "20210311111503");
user_pref("browser.startup.homepage_override.mstone", "86.0.1");
user_pref("browser.startup.lastColdStartupCheck", 1615903238);
user_pref("browser.tabs.closeWindowWithLastTab", false);
user_pref("browser.tabs.disableBackgroundZombification", false);
user_pref("browser.tabs.remote.separatePrivilegedContentProcess", false);
user_pref("browser.tabs.warnOnClose", false);
user_pref("browser.tabs.warnOnCloseOtherTabs", false);
user_pref("browser.tabs.warnOnOpen", false);
user_pref("browser.toolbars.bookmarks.visibility", "never");
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"customizableui-special-spring2\",\"downloads-button\",\"library-button\",\"sidebar-button\",\"fxa-toolbar-menu-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"import-button\",\"personal-bookmarks\"]},\"seen\":[\"developer-button\"],\"dirtyAreaCache\":[\"nav-bar\",\"PersonalToolbar\"],\"currentVersion\":16,\"newElementCount\":2}");
user_pref("browser.uitour.enabled", false);
user_pref("browser.urlbar.placeholderName", "Google");
user_pref("browser.urlbar.suggest.searches", false);
user_pref("browser.usedOnWindows10.introURL", "");
user_pref("browser.warnOnQuit", false);
user_pref("datareporting.healthreport.documentServerURI", "http://%(server)s/dummy/healthreport/");
user_pref("datareporting.healthreport.logging.consoleEnabled", false);
user_pref("datareporting.healthreport.service.enabled", false);
user_pref("datareporting.healthreport.service.firstRun", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("datareporting.policy.dataSubmissionPolicyAccepted", false);
user_pref("datareporting.policy.dataSubmissionPolicyBypassNotification", true);
user_pref("distribution.archlinux.bookmarksProcessed", true);
user_pref("distribution.iniFile.exists.appversion", "86.0.1");
user_pref("distribution.iniFile.exists.value", true);
user_pref("doh-rollout.balrog-migration-done", true);
user_pref("doh-rollout.doneFirstRun", true);
user_pref("dom.disable_beforeunload", true);
user_pref("dom.disable_open_during_load", false);
user_pref("dom.file.createInChild", true);
user_pref("dom.ipc.reportProcessHangs", false);
user_pref("dom.max_script_run_time", 0);
user_pref("dom.push.connection.enabled", false);
user_pref("extensions.activeThemeID", "default-theme@mozilla.org");
user_pref("extensions.autoDisableScopes", 0);
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.databaseSchema", 33);
user_pref("extensions.enabledScopes", 5);
user_pref("extensions.getAddons.cache.enabled", false);
user_pref("extensions.getAddons.databaseSchema", 6);
user_pref("extensions.getAddons.discovery.api_url", "data:, ");
user_pref("extensions.incognito.migrated", true);
user_pref("extensions.installDistroAddons", false);
user_pref("extensions.lastAppBuildId", "20210311111503");
user_pref("extensions.lastAppVersion", "86.0.1");
user_pref("extensions.lastPlatformVersion", "86.0.1");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.update.enabled", false);
user_pref("extensions.update.notifyUser", false);
user_pref("extensions.webcompat.enable_picture_in_picture_overrides", true);
user_pref("extensions.webcompat.enable_shims", true);
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.screenshots@mozilla.org", true);
user_pref("extensions.webextensions.uuids", "{\"doh-rollout@mozilla.org\":\"b67017e0-5218-4a63-8c49-2afa55395c59\",\"formautofill@mozilla.org\":\"562a0480-47a1-4c60-9c4a-b8fd96889909\",\"screenshots@mozilla.org\":\"03bc36db-4b0c-4459-9a9e-867093f1ef2f\",\"webcompat-reporter@mozilla.org\":\"399a35d0-21a6-4033-95c8-88f3215962ac\",\"webcompat@mozilla.org\":\"7ded40ac-c619-480b-940f-8bf9c743c060\",\"default-theme@mozilla.org\":\"11969563-97be-47c1-9c32-0759917e11e0\",\"google@search.mozilla.org\":\"1443fce7-648f-4a7a-8774-c776f4bb592b\",\"amazondotcom@search.mozilla.org\":\"b7856e30-6ada-4185-88d7-cf62d439b0d5\",\"wikipedia@search.mozilla.org\":\"248c02df-3dfe-41f4-970f-354521f4c8eb\",\"bing@search.mozilla.org\":\"0a60ae98-167d-401f-974e-82ca9f0d110a\",\"ddg@search.mozilla.org\":\"8a39726f-f1e5-4158-bf6e-514b016049d4\"}");
user_pref("fission.experiment.max-origins.last-disqualified", 0);
user_pref("fission.experiment.max-origins.last-qualified", 1615903241);
user_pref("fission.experiment.max-origins.qualified", true);
user_pref("focusmanager.testmode", true);
user_pref("general.useragent.updates.enabled", false);
user_pref("geo.provider.testing", true);
user_pref("geo.wifi.scan", false);
user_pref("marionette.enabled", true);
user_pref("media.gmp.storage.version.observed", 1);
user_pref("network.http.phishy-userpass-length", 255);
user_pref("network.manage-offline-status", false);
user_pref("network.proxy.type", 0);
user_pref("network.sntp.pools", "%(server)s");
user_pref("network.trr.blocklist_cleanup_done", true);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("privacy.sanitize.pending", "[]");
user_pref("security.certerrors.mitm.priming.enabled", false);
user_pref("security.fileuri.strict_origin_policy", false);
user_pref("security.notification_enable_delay", 0);
user_pref("security.sandbox.content.tempDirSuffix", "ecf12a11-ed0d-4386-ba2d-d99f7a97f12a");
user_pref("security.sandbox.plugin.tempDirSuffix", "3c4ec7f5-6c01-40a7-8787-d58eb59bb706");
user_pref("services.settings.server", "http://%(server)s/dummy/blocklist/");
user_pref("services.sync.clients.lastSync", "0");
user_pref("services.sync.declinedEngines", "");
user_pref("services.sync.globalScore", 0);
user_pref("services.sync.nextSync", 0);
user_pref("services.sync.tabs.lastSync", "0");
user_pref("signon.autofillForms", false);
user_pref("signon.rememberSignons", false);
user_pref("startup.homepage_welcome_url", "about:blank");
user_pref("toolkit.startup.last_success", 1615903236);
user_pref("toolkit.startup.max_resumed_crashes", -1);
user_pref("toolkit.telemetry.cachedClientID", "c0ffeec0-ffee-c0ff-eec0-ffeec0ffeec0");
user_pref("toolkit.telemetry.previousBuildID", "20210311111503");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("trailhead.firstrun.didSeeAboutWelcome", true);
