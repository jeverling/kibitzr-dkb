## kibitzr check for DKB transactions

DKB is a German bank, that unfortunately doesn't offer any kind of notifications for transactions.
This project uses [kibitzr](https://github.com/kibitzr/kibitzr), to periodically monitor the DKB website and send an email when the current balance has changed.

To be able to use environment variables for credentials, it is using a [fork of kibitzr](https://github.com/jeverling/kibitzr/tree/use-yamlenv-to-interpolate-variables), that uses a proof of concept approach to interpolate environment variables in `kibitzr-creds.yml` using [yamlenv](https://github.com/lbolla/yamlenv).
This works well enough for my use-case, but there are some caveats currently as discussed in [kibitzr#37](https://github.com/kibitzr/kibitzr/issues/37).

As soon as there is proper support for environment variable interpolation this project should be updated.


### Configuration

To set up username/password for your DKB account and configure email sending, you can set the following variables:

*   `KIBITZR_DKB_USERNAME`
*   `KIBITZR_DKB_PASSWORD`
*   `KIBITZR_SMTP_HOST`
*   `KIBITZR_SMTP_PORT`
*   `KIBITZR_SMTP_USER`
*   `KIBITZR_SMTP_PASSWORD`

Note that the WIP fork for environment variable interpolation currently only interpolates variables in `kibitzr-creds.yml`, so if you want to use this project for your DKB account you will probably want to change the recipient email address in `kibitzr.yml`. ;-)


### Output

Unfortunately the list of transfers on the DBK website is in seemingly arbitrary order (not ordered properly by e.g. date), so it's not easily possible to determine the latest transfer. Therefore this project will only output the current balance, and not include the latest transfer.
