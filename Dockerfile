FROM python:3.9-slim

LABEL MAINTAINER Jesaja Everling <jeverling@gmail.com>

# install Firefox and geckodriver
RUN apt-get update && \
    apt-get install -y firefox-esr curl git && \
    apt-get clean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN pip install git+https://github.com/jeverling/kibitzr@use-yamlenv-to-interpolate-variables#egg=kibitzr yamlenv

RUN curl -L https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz | tar xfz - && \
    mv geckodriver /usr/local/bin/

RUN useradd -r -m kibitzr

USER kibitzr

WORKDIR /home/kibitzr

# wasn't working, probably due to cookie warning
# (got a "Vorschaltseite").
# Persistent profile should help, unfortunately it's not so easy
# because geckodriver firefox defaults to "system proxy settings" which
# don't work, doh
COPY kibitzr.yml kibitzr-creds.yml firefox_profile /home/kibitzr/

CMD kibitzr run
